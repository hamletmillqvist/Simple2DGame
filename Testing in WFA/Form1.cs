﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Collections.Generic;

namespace Testing_in_WFA
{
    public partial class MainFrame : Form
    {
        //Det gamla systemet för att rita banor.
        /*static int[] tileList =
        {
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 2, 3, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1,
                2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1
        };

        static int[] objectList =
            {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };*/

        //Välj om programmet skall köras i DEBUG.
        public static bool DEBUG = false;

        //Ett enum för att låta spelaren göra val.
        public enum eChoice { none, pickup, recall }
        Dictionary<eChoice, string[]> choiceAlternatives = new Dictionary<eChoice, string[]>()
        {
            { eChoice.none, new string[] { "No", "Yes" } },
            { eChoice.pickup, new string[] { "Leave item on ground.", "Pick up item." } },
            { eChoice.recall, new string[] { "Stay on map.", "Recall character to base." } },
        };


        //Gör ett val som skall spara nuvarande valet.
        eChoice ongoingChoice;

        //Generera alla tileObjekt och inventoryObjekt som skall vara tillängliga.
        //TODO - Behöver göra en fil för att generera alla objekt när det väl finns fler.
        static inventoryObject ioSTICKSTACK = new inventoryObject("Stickstack", "A bunch of sticks in a stack.", new Bitmap("src/stickstack.png"), inventoryObject.eTargetType.WaterTile );

        TileObject nil = new TileObject("nil", "nil");
        TileObject toSTICKSTACK = new TileObject("Stickstack", "a bunch of sticks in a stack.", new Bitmap("src/stickstack.png"), true, ioSTICKSTACK);
        TileObject PlayerHomebase = new TileObject("Homebase", "a player-faction controlled homebase.", new Bitmap("src/spawnFlag.png"));
        TileObject FriendlyHomebase = new TileObject("Friendly Homebase", " friendly factions homebase.", new Bitmap("src/spawnFlag.png"));
        TileObject NeutralHomeBase = new TileObject("Neutral Homebase", "a non-controlled homebase.", new Bitmap("src/spawnFlag.png"));
        TileObject HostileHomebase = new TileObject("Hostile Homebase", "a hostile factions homebase.", new Bitmap("src/spawnFlag.png"));

        //Laddar startbana.
        //TODO - Gör en huvudmeny där man kan välja vilken bana som skall laddas.
        Map currentMap = Map.loadFile("level1"); //new Map(20, 15, tileList, objectList);

        //Huvudfönstret
        public MainFrame()
        {
            InitializeComponent();
            
            //Sätter GUI att visa nuvarande karta.
            GUI.setMap(currentMap);

            //Ritar canvas (karta) på start-laddning.
            canvas.Paint += Canvas_Paint;

            MessageBox.Show("Hello and welcome to SIMPLE2DGAME!\n\nMove with \"UP, DOWN, RIGHT, LEFT\"\nInteract with \"INTERACT button.\"\nOpen inventory with \"INVENTORY button.\"\n\nThere are currently 3 pre-made maps [level1], [flat_grass] and [space].\nYou can save custom maps by editing an existing map, pick a name, then click \"EXPORT\"!\n\nTo launch a map in DEBUG type \"[LEVELNAME]_debug\" and press \"Import\".", "Instructions");

        }

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            //Ritar kartan på kanvas.
            currentMap.draw(canvas);
        }

        // TODO - Behöver göra en bättre kartritare, mycket svårt att göra en karta just nu.
        private void btnDraw_Click(object sender, EventArgs e)
        {
            //Sätter standart-tile till void. (Inget)
            int i = 0;

            //Om grön är checkad, sätt tile till gräs...
            if (isGreen.Checked)
                i = 1;
            //.. annars on blå är checkad, sätt tile till vatten.
            else if (isBlue.Checked)
                i = 2;

            //Byter ut all information om tilen, namn, beskrivning, et cetera.
            currentMap.tileList[(int) numY.Value * currentMap.width + (int)numX.Value].generateInfo(i);

            //Uppdaterar kartan.
            currentMap.draw(canvas);
        }
        
        //Knappar för att röra karaktären.
        //TODO - Skall bytas ut mot det nya rörelse-systemet med musen.
        private void btnUp_Click(object sender, EventArgs e)
        {
            lblWaterWarning.Visible = false;

            Player.Rotate(Player.direction.Up);

            if (Player.posY != 0)
            {
                if (currentMap.getTile(Player.posX, Player.posY - 1).isPassable() || currentMap.getTile(Player.posX, Player.posY - 1).interactable.isPassable())
                {
                    Player.posY = Player.posY - 1;
                }
                else
                {
                    lblWaterWarning.Visible = true;
                }
            }

            currentMap.draw(canvas);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            lblWaterWarning.Visible = false;

            Player.Rotate(Player.direction.Down);

            if (Player.posY != currentMap.height - 1)
            {
                if (currentMap.getTile(Player.posX, Player.posY + 1).isPassable() || currentMap.getTile(Player.posX, Player.posY + 1).interactable.isPassable())
                {
                    Player.posY = Player.posY + 1;
                }
                else
                {
                    lblWaterWarning.Visible = true;
                }
            }

            currentMap.draw(canvas);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            lblWaterWarning.Visible = false;

            Player.Rotate(Player.direction.Left);

            if (Player.posX != 0)
            {
                if (currentMap.getTile(Player.posX - 1, Player.posY).isPassable() || currentMap.getTile(Player.posX - 1, Player.posY).interactable.isPassable())
                {
                    Player.posX = Player.posX - 1;
                }
                else
                {
                    lblWaterWarning.Visible = true;
                }
            }

            currentMap.draw(canvas);
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            lblWaterWarning.Visible = false;

            Player.Rotate(Player.direction.Right);

            if (Player.posX != currentMap.width - 1)
            {
                if (currentMap.getTile(Player.posX + 1, Player.posY).isPassable() || currentMap.getTile(Player.posX + 1, Player.posY).interactable.isPassable())
                {
                    Player.posX = Player.posX + 1;
                }
                else
                {
                    lblWaterWarning.Visible = true;
                }
            }
                

            currentMap.draw(canvas);
        }

        //Interaktera med tileObject.
        private void btnInteract_Click(object sender, EventArgs e)
        {
            //Skriv ut information om den tile spelaren står på i informationsrutan.
            txtBoxUpdates.Text = (currentMap.getTile(Player.posX, Player.posY).info);

            //Kollar så att spelaren står på ett objekt på kartan.
            if (currentMap.getTile(Player.posX, Player.posY).interactable != TileObject.tileObjectList[0])
            {
                //Skriv ut information om det objekt spelaren står på i informationsrutan istället.
                if (currentMap.getTile(Player.posX, Player.posY).interactable.convert() != null)
                    txtBoxUpdates.Text = (currentMap.getTile(Player.posX, Player.posY).interactable.convert().desc);

                //Kollar så att spelarens inventarie inte är fullt och att objekt får att plocka upp.
                if (Player.inventory.Contains(null) && currentMap.getTile(Player.posX, Player.posY).interactable.pickup == true)
                {
                    //Skriver ut text på informationsrutan om spelaren vill plocka upp objektet.
                    txtBoxUpdates.Text += Environment.NewLine + Environment.NewLine + "Pick up item? ";

                    //Ger spelaren ett val om att plocka upp objektet.
                    choice(eChoice.pickup);
                }
                //Om det inte finns plats, skriv att det inte finns plats.
                else if (Player.inventory.Contains(null) == false && currentMap.getTile(Player.posX, Player.posY).interactable.pickup == true)
                {
                    txtBoxUpdates.Text += Environment.NewLine + Environment.NewLine + "There is no space to pick up this item.";
                }
                //Går igenom alla tileObject interactioner.
                else if (currentMap.getTile(Player.posX, Player.posY).interactable.pickup == false)
                {
                    if (currentMap.getTile(Player.posX, Player.posY).interactable.name == "Homebase")
                    {
                        if (Player.onField)
                        {
                            txtBoxUpdates.Text = "Do you want to recall your character? ";

                            choice(eChoice.recall);
                        }
                    }
                }
            }
        }

        //Växlar mellan inventarie synligt och osynligt.
        private void bntInventory_Click(object sender, EventArgs e)
        {
            GUI.inventory();

            if (GUI.inventory_Open)
            {
                txtBoxUpdates.Text = "Inventory: " + Environment.NewLine;

                foreach (inventoryObject io in Player.inventory)
                {
                    if (io != null)
                    {
                        txtBoxUpdates.Text += Environment.NewLine + io.name + " - " + io.desc;
                    }
                }
                foreach (inventoryObject io in Player.inventory)
                {
                    if (io == null)
                    {
                        txtBoxUpdates.Text += Environment.NewLine + "[OPEN SLOT]";
                    }
                }
            }
            else
            {
                txtBoxUpdates.Text = "Closed inventory.";
            }

            currentMap.draw(canvas,1);
        }
        
        //Ändrar vilka knappar som går att kolla på om spelaren skall göra ett val.
        public void choice(eChoice choice, char c = 'x')
        {
            //Om inget pågåendeval mottas, stäng av alla knappar och sätt på JA/NEJ knappar.
            if (c == 'x')
            {
                btnUp.Enabled = false;
                btnDown.Enabled = false;
                btnRight.Enabled = false;
                btnLeft.Enabled = false;
                btnInteract.Enabled = false;
                btnInventory.Enabled = false;

                btnChoiceNo.Enabled = true;
                btnChoiceYes.Enabled = true;

                //Sätt nuvarande val till det val som skickades.
                ongoingChoice = choice;

                //Byt ut texten på knapparna till frågans svar.
                btnChoiceNo.Text = choiceAlternatives[ongoingChoice][0];
                btnChoiceYes.Text = choiceAlternatives[ongoingChoice][1];

                //Byter focus till "JA"-knappen.
                btnChoiceYes.Select();
            }
            //Kollar om spelaren vill plocka upp ett objekt
            else if (choice == eChoice.pickup)
            {
                if (c == 'y') //JA
                {
                    //Skriver uppdatering 
                    txtBoxUpdates.Text = "Picked up " + currentMap.getTile(Player.posX, Player.posY).interactable.name + ".";

                    //Plocka upp objekt (tar bort ifrån kartan)
                    //TODO - Gör objekt som kan plockas upp oändligt många gånger.
                    Player.pickup(currentMap.getTile(Player.posX, Player.posY));

                    ongoingChoice = eChoice.none;
                }
                else if (c == 'n') //NEJ
                {
                    //Skriver uppdatering.
                    txtBoxUpdates.Text = "Left " + currentMap.getTile(Player.posX, Player.posY).interactable.name + " on the ground.";

                    ongoingChoice = eChoice.none;
                }
            }
            //Kollar om spelaren vill kalla tillbaka en karaktär.
            else if (choice == eChoice.recall)
            {
                if (c == 'y')
                {
                    txtBoxUpdates.Text = "Character has been recalled to the homebase. (this does nothing right now)";

                    ongoingChoice = eChoice.none;
                }
                else if (c == 'n')
                {
                    txtBoxUpdates.Text = "Character stays on the field.";

                    ongoingChoice = eChoice.none;
                }
            }

            //Startar alla knappar igen om ett val gjorts
            if (c != 'x' && ongoingChoice == eChoice.none)
            {
                //Återställer alla knappar.
                btnUp.Enabled = true;
                btnDown.Enabled = true;
                btnRight.Enabled = true;
                btnLeft.Enabled = true;
                btnInteract.Enabled = true;
                btnInventory.Enabled = true;

                btnChoiceNo.Enabled = false;
                btnChoiceYes.Enabled = false;

                btnChoiceNo.Text = choiceAlternatives[ongoingChoice][0];
                btnChoiceYes.Text = choiceAlternatives[ongoingChoice][1];
            }

            //Updaterar kartan.
            currentMap.draw(canvas);
        }

        private void btnChoiceNo_Click(object sender, EventArgs e)
        {
            //Skickar NEJ till nuvarande val.
            choice(ongoingChoice, 'n');
        }

        private void btnChoiceYes_Click(object sender, EventArgs e)
        {
            //Skickar JA till nuvarande val.
            choice(ongoingChoice, 'y');
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //Sparar en fil (eller skriver över existerande) för nuvarande karta.
            FileStream file = new FileStream("res/" + txtBoxTargetMap.Text + ".xml", FileMode.Create);

            //Skapar en filström att skriva till filen igenom.
            StreamWriter fs = new StreamWriter(file);

            //Använder en skrivare för XML-Dokument.
            using (XmlTextWriter xmlWriter = new XmlTextWriter(fs))
            {
                //Sätter positionering på element (antalet mellanslag)
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.Indentation = 4;

                //Skapar variabler för att memorera vilken sort utav tile eller objekt det är på tilen.
                int tileID = 0, objectID = 0;

                //Försöker genomgå skrivning till dokument.
                //TODO - Gör en tempfil som raderas om det går fel, istället för att riskera korreupterade sparfiler.
                try
                {
                    //Skriver starten på xml-dokumentet.
                    xmlWriter.WriteStartDocument();

                    //Skapar ett kart-element.
                    xmlWriter.WriteStartElement("Map");

                    //Skriver ut attribut för huvudelementet. (Namn och storlek)
                    xmlWriter.WriteAttributeString("name", txtBoxTargetMap.Text);
                    xmlWriter.WriteAttributeString("width", currentMap.width.ToString());
                    xmlWriter.WriteAttributeString("height", currentMap.height.ToString());

                    //Kollar varje tile på kartan.
                    foreach (Tile t in currentMap.tileList)
                    {
                        //Återställer texten s.
                        string s = "nil";

                        //Skapa ett tile-element, och ge ett ID attribut för att beskriva typen. Spara namnet på typen i textformat också.
                        xmlWriter.WriteStartElement("Tile");
                        xmlWriter.WriteAttributeString("ID", tileID.ToString());
                        xmlWriter.WriteElementString("TileType", t.name);

                        //Skapar ett objekt-element.
                        xmlWriter.WriteStartElement("Interactable");

                        //Kollar igenom alla möjliga objekt, och försöker hitta en matchning. Sparar ID på matchen.
                        for (int j = 1; j < TileObject.tileObjectList.ToArray().Length; j++)
                        {
                            if (t.interactable.name == TileObject.tileObjectList[j].name && t.interactable.desc == TileObject.tileObjectList[j].desc)
                                objectID = j;
                        }

                        //Skriver ut attribut för objekt-elementet. (ID för att spara vilket sorts element är där)
                        xmlWriter.WriteAttributeString("ID", objectID.ToString());

                        //Byter text på pickup OM det finns någonting att plocka upp.
                        if (t.interactable.convert() != null)
                            s = t.interactable.convert().name;
                        xmlWriter.WriteAttributeString("Pickup", s);
                        xmlWriter.WriteEndElement(); //Avslutar element [OBJECT-ELEMENT]

                        //Sparar information om tile går att passera.
                        xmlWriter.WriteElementString("Trasspassable", t.isPassable().ToString());
                        xmlWriter.WriteEndElement(); //Avlutar element [TILE]

                        //Öker tileID med ett. 
                        tileID++;

                        //Återställer objektID.
                        objectID = 0;
                    }
                    xmlWriter.WriteEndElement(); //Avlutar element [KARTA]

                    //Skriver ut slutinformation till dokumentet och stänger ned dokumentet.
                    xmlWriter.WriteEndDocument();

                    //Visar popup-ruta att exportering slutfördes utan problem.
                    MessageBox.Show("Level Exported", "SUCCESS");                 
                } catch
                {
                    //Visar popup-ruta att någonting gick fel vid export.
                    MessageBox.Show("Could not export data... :(", "ERROR");

                    //Fråga om den korrupta filen skall sparas eller raderas.
                    //TODO - Skapa JA/NEJ formulär för att spara korrupt fil, och skriva ut error-log.
                }
            }
            //Stänger filström och filen.
            fs.Close();
            file.Close();

            //Kastar filhanteraren.
            fs.Dispose();
            file.Dispose();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            //Kollar om banan laddades med DEBUG
            if (txtBoxTargetMap.Text.EndsWith("_debug"))
            {
                DEBUG = true;

                txtBoxTargetMap.Text = txtBoxTargetMap.Text.Substring(0, txtBoxTargetMap.Text.LastIndexOf("_debug"));
            }
            else
            {
                DEBUG = false;
            }

            //Försök att ladda banan ifrån dokument.
            try
            {
                currentMap = Map.loadFile(txtBoxTargetMap.Text);
            } catch
            {
                //Visa popup-ruta att det inte fanns en bana med det namnet. (Banan kan också vara korrupt)
                MessageBox.Show("Could not find a map named \"" + txtBoxTargetMap.Text + "\" or the file was corrupt.", "ERROR");

                //Avbryter
                return;
            }

            //Flytta spelaren till nya spawn.
            Player.posX = currentMap.Homebase.X;
            Player.posY = currentMap.Homebase.Y;

            //Uppdaterar banan (Om den lyckades ladda)
            currentMap.draw(canvas);

            //Skriver ut text på informationsrutan att en ny bana laddades.
            txtBoxUpdates.Text = "Loaded map \"" + txtBoxTargetMap.Text + "\", SUCCESS.";
            if (DEBUG)
            {
                txtBoxUpdates.Text += Environment.NewLine + "DEBUG MODE ACIVATED!";
            }
            

            //Visa popup-ruta att banan laddades.
            MessageBox.Show("Loaded map \"" + currentMap.name + "\"\n\nDebug: " + DEBUG, "SUCCESS");

        }

        //Rita ett sträck från spelare till muspekarens 40x40 tile.
        private Point mouseLocation = new Point(0, 0);
        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            //Sätter variabler för att veta vilken tile (vilka koordinater) som musen befinner sig i.
            int x = e.X / 40;
            int y = e.Y / 40;

            //Kollar om musen har flyttat sig till en annan tile, och i sådana fall ritar om.
            if (x != mouseLocation.X || y != mouseLocation.Y)
            {
                mouseLocation.X = x;
                mouseLocation.Y = y;

                //Ritar om kartan för att göra sig av med alla gamla sträck.
                currentMap.draw(canvas);
                Graphics g = canvas.CreateGraphics();

                //TODO - Flytta all kod efter detta till GUI generatorn.
                if (GUI.inventory_Open == false)
                {
                    //Räknar ut distans ifrån spelare till muspekarens tile.
                    int deltaX = x - Player.posX;
                    int deltaY = y - Player.posY;

                    //Räknar ut distans med pythagoras sats. (a^2 + b^2 = c^2)
                    double avgDistance = Math.Round(Math.Sqrt(deltaX * deltaX + deltaY * deltaY), 2);

                    //Skriver ut information om den tile som muspekaren är ovanpå (INFO BEROR PÅ DEBUG)
                    if (DEBUG)
                    {
                        //Skriver ut alla värden och information.
                        txtBoxUpdates.Text = "Coordinates (x,y): (" + deltaX + ", " + deltaY + ")";
                        txtBoxUpdates.Text += Environment.NewLine + "Avarage blocks distance: " + avgDistance;

                        //Skriver ut namn på tile och objekt.
                        txtBoxUpdates.Text += Environment.NewLine + Environment.NewLine + "Target Tile (ID " + currentMap.getTileID(x, y) + "): " + currentMap.getTile(x, y).name;
                        if (currentMap.getTile(x, y).interactable != TileObject.tileObjectList[0])
                            txtBoxUpdates.Text += " with " + currentMap.getTile(x, y).interactable.name;

                        //Kolla om muspekarens tile är passerbar.
                        txtBoxUpdates.Text += Environment.NewLine + "Passable: " + currentMap.getTile(x, y).isPassable();

                        //Skriver ut alla möjliga drag ifrån den tilen.
                        txtBoxUpdates.Text += Environment.NewLine + Environment.NewLine + "---------------- Adjacent tiles ----------------" + Environment.NewLine;

                        int i = 1;
                        foreach (Tile adjacent in currentMap.getAdjacentTiles(currentMap.getTileID(mouseLocation.X, mouseLocation.Y)))
                        {

                            txtBoxUpdates.Text += Environment.NewLine;

                            switch (i)
                            {
                                case 1:
                                    txtBoxUpdates.Text += "\tRight\t";
                                    if (adjacent.name != "void")
                                    {
                                        txtBoxUpdates.Text += adjacent.name + "\t";
                                        if (adjacent.isPassable())
                                        {
                                            g.DrawLine(new Pen(Color.Magenta), mouseLocation.X * 40 + 40, mouseLocation.Y * 40 + 20, mouseLocation.X * 40 + 20 + 40, mouseLocation.Y * 40 + 20);
                                        }
                                        else
                                        {
                                            txtBoxUpdates.Text += "Not ";
                                        }
                                        txtBoxUpdates.Text += "Trasspassable";

                                    }
                                    else
                                    {
                                        txtBoxUpdates.Text += "";
                                    }
                                    break;
                                case 2:
                                    txtBoxUpdates.Text += "\tDown\t";
                                    if (adjacent.name != "void")
                                    {
                                        txtBoxUpdates.Text += adjacent.name + "\t";
                                        if (adjacent.isPassable())
                                        {
                                            g.DrawLine(new Pen(Color.Magenta), mouseLocation.X * 40 + 20, mouseLocation.Y * 40 + 40, mouseLocation.X * 40 + 20, mouseLocation.Y * 40 + 20 + 40);
                                        }
                                        else
                                        {
                                            txtBoxUpdates.Text += "Not ";
                                        }
                                        txtBoxUpdates.Text += "Trasspassable";
                                    }
                                    else
                                    {
                                        txtBoxUpdates.Text += "";
                                    }
                                    break;
                                case 3:
                                    txtBoxUpdates.Text += "\tLeft\t";
                                    if (adjacent.name != "void")
                                    {
                                        txtBoxUpdates.Text += adjacent.name + "\t";
                                        if (adjacent.isPassable())
                                        {
                                            g.DrawLine(new Pen(Color.Magenta), mouseLocation.X * 40, mouseLocation.Y * 40 + 20, mouseLocation.X * 40 - 20, mouseLocation.Y * 40 + 20);
                                        }
                                        else
                                        {
                                            txtBoxUpdates.Text += "Not ";
                                        }
                                        txtBoxUpdates.Text += "Trasspassable";
                                    }
                                    else
                                    {
                                        txtBoxUpdates.Text += "";
                                    }
                                    break;
                                case 4:
                                    txtBoxUpdates.Text += "\tUp\t";
                                    if (adjacent.name != "void")
                                    {
                                        txtBoxUpdates.Text += adjacent.name + "\t";
                                        if (adjacent.isPassable())
                                        {
                                            g.DrawLine(new Pen(Color.Magenta), mouseLocation.X * 40 + 20, mouseLocation.Y * 40, mouseLocation.X * 40 + 20, mouseLocation.Y * 40 - 20);
                                        }
                                        else
                                        {
                                            txtBoxUpdates.Text += "Not ";
                                        }
                                        txtBoxUpdates.Text += "Trasspassable";
                                    }
                                    else
                                    {
                                        txtBoxUpdates.Text += "";
                                    }
                                    break;
                            }

                            i++;
                        }
                    }
                    else
                    {
                        //Skriver ut namn på tile och objekt.
                        txtBoxUpdates.Text = currentMap.getTile(x, y).name;
                        if (currentMap.getTile(x, y).interactable != TileObject.tileObjectList[0])
                            txtBoxUpdates.Text += " with " + currentMap.getTile(x, y).interactable.desc;

                        //Skriver ut distans.
                        txtBoxUpdates.Text += Environment.NewLine + Math.Round(avgDistance, 0) + " block(s) away.";

                    }

                    //Ritar en rektangel runt den fyrkant som muspekaren är på.
                    g.DrawRectangle(new Pen(Color.Red), x * 40, y * 40, 40, 40);

                    //Ritar en linje till fyrkanten.
                    g.DrawLine(new Pen(Color.Red), Player.posX * 40 + 20, Player.posY * 40 + 20, x * 40 + 20, y * 40 + 20);

                    if (1 <= avgDistance)
                    {
                        //Skriver text i fyrkanten.
                        g.DrawString("Move", MainFrame.DefaultFont, new SolidBrush(Color.Black), x * 40 + 5, y * 40 + 15);
                    }
                }
                else //Man kan inte röra sig medan inventory är öppen.
                {
                    if (mouseLocation.X == Player.posX + 2 && mouseLocation.Y == Player.posY + 1)
                    {
                        Pen p;
                        if (Player.inventory[0] != null)
                        {
                            p = new Pen(Color.Yellow);
                        }
                        else
                        {
                            p = new Pen(Color.Red);
                        }

                        g.DrawRectangle(p, mouseLocation.X * 40, mouseLocation.Y * 40, 40, 40);
                        

                        p.Dispose();
                    }
                    else if (mouseLocation.X == Player.posX + 3 && mouseLocation.Y == Player.posY + 1)
                    {
                        Pen p;
                        if (Player.inventory[1] != null)
                        {
                            p = new Pen(Color.Yellow);
                        }
                        else
                        {
                            p = new Pen(Color.Red);
                        }

                        g.DrawRectangle(p, mouseLocation.X * 40, mouseLocation.Y * 40, 40, 40);

                        p.Dispose();
                    }
                    else if (mouseLocation.X == Player.posX + 2 && mouseLocation.Y == Player.posY + 2)
                    {
                        Pen p;
                        if (Player.inventory[2] != null)
                        {
                            p = new Pen(Color.Yellow);
                        }
                        else
                        {
                            p = new Pen(Color.Red);
                        }

                        g.DrawRectangle(p, mouseLocation.X * 40, mouseLocation.Y * 40, 40, 40);

                        p.Dispose();
                    }
                    else if (mouseLocation.X == Player.posX + 3 && mouseLocation.Y == Player.posY + 2)
                    {
                        Pen p;
                        if (Player.inventory[3] != null)
                        {
                            p = new Pen(Color.Yellow);
                        }
                        else
                        {
                            p = new Pen(Color.Red);
                        }

                        g.DrawRectangle(p, mouseLocation.X * 40, mouseLocation.Y * 40, 40, 40);

                        p.Dispose();
                    }
                }

                
                
                //Kasta den grafiska ritaren.
                g.Dispose();
            }
        }

        private void canvas_MouseClick(object sender, EventArgs e)
        {
            if (DEBUG)
            {
                MessageBox.Show("Walk function via mouse is not fully functional yet!\n\nStill, player will move there now.", "Placeholder Method");
                Player.posX = mouseLocation.X; Player.posY = mouseLocation.Y;
                currentMap.draw(canvas, 1);
            }
            /*if (GUI.inventory_Open == false)
            {
                List<int> path = currentMap.findPath(currentMap.getTileID(Player.posX, Player.posY), currentMap.getTileID(mouseLocation.X, mouseLocation.Y));
                txtBoxUpdates.Text = "Path (ID): ";
                foreach (int i in path)
                {
                    txtBoxUpdates.Text += "["+i+"],";
                }
            }*/
        }
    }
}
