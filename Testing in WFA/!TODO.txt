====== GUI ======
* Mouse over inventory should not show movement box.
* Mousing over inventory objects should show name,
	and clicking should use item.
* Displaying Path from Player-object to target-tile via tiles. (Red Line)
* Better Player-object sprite.
* Display controlled territories by factions. (World map)

====== GAMEPLAY ======
* Building bridges with stickstack. (Inventory)
* Max movement tiles for Player-object.
* "Next Turn" button.
* Mutliple Player-objects. (Characters)
* Moving between scenes (levels/maps) via doors, roads etc. (Portals)
* Add a world map, a larger view of different levels/maps/scenes.

====== Controls ======
* Movement on mouseclick. (If path is available and withint max
	movement tiles this round.)
	
====== Utilities ======
* Map funktion to grab adjacent tiles. (Linked Tiles)
* Find a path of linked tiles from Player-object to target-tile. (Mouseover)
* Saving mouseover tiles in order to create a custom path.
* Level Editor application.
* tileObject sprites with animations.